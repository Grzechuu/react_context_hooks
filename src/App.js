import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Login from './container/LoginContainer';
import Home from './container/HomeContainer';
import Navbar from './components/Navbar';
import './App.css';
import AuthRote from './AuthRoute';

class App extends Component {
  render() {
    return (
      <Router>
        <Navbar />
        <Switch>
          <AuthRote component={Home} path="/Home" />
          <Route component={Login} path="/" />
        </Switch>
      </Router>
    );
  }
}

export default App;
