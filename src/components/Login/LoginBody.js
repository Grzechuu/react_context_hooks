import React, { useState, useContext } from 'react';
import { Button, Form } from 'react-bulma-components/full';
import styled from 'styled-components';
import { LoginContext } from '../../context/LoginContext';
import userFormInput from '../../hooks/useFormInput';

const TextInput = styled(Form.Input)`
  margin: 5px 0 5px 0;
  border-color: ${props => (props.isValid ? null : 'red')};
`;
const SubmitButton = styled(Button)`
  background-color: #1a5e63;
  color: #fff;
  margin-top: 15px;
`;

const LoginBody = () => {
  const context = useContext(LoginContext);
  const loginPassword = userFormInput('');
  const loginEmail = userFormInput('');
  const registerEmial = userFormInput('');
  const registerPassword = userFormInput('');
  const registerPasswordConfirmation = userFormInput('');

  const [registerEmailValid, setRegisterEmailValid] = useState(true);
  const [registerPasswordValid, setRegisterPasswordValid] = useState(true);
  const [registerPasswordConfirmationValid, setRegisterPasswordConfirmationValid] = useState(true);
  const [loginEmailValid, setLoginEmailValid] = useState(true);
  const [loginPasswordValid, setLoginPasswordValid] = useState(true);

  const checkRegisterInputsValid = (email, password, passwordConfirmation) => {
    const emailValid = email;
    const passwordValid = password;
    const passwordConfirmationValid = !!passwordConfirmation;
    setRegisterEmailValid(emailValid);
    setRegisterPasswordValid(passwordValid);
    setRegisterPasswordConfirmationValid(passwordConfirmationValid);
    return emailValid && passwordValid && passwordConfirmationValid;
  };

  const checkLoginInputsValid = (email, password) => {
    const emailValid = email;
    const passwordValid = password;
    setLoginEmailValid(emailValid);
    setLoginPasswordValid(passwordValid);
    return emailValid && passwordValid;
  };

  if (context.activeLogin) {
    return (
      <Form.Field>
        <TextInput type="text" placeholder="E-mail" isValid={loginEmailValid} {...loginEmail} />
        <TextInput type="password" placeholder="Password" isValid={loginPasswordValid} {...loginPassword} />
        <SubmitButton fullwidth onClick={() => checkLoginInputsValid(loginEmail.value, loginPassword.value)}>
          Login
        </SubmitButton>
      </Form.Field>
    );
  }
  if (context.activeRegister) {
    return (
      <Form.Field>
        <TextInput type="text" placeholder="E-mail" isValid={registerEmailValid} {...registerEmial} />
        <TextInput type="password" placeholder="Password" isValid={registerPasswordValid} {...registerPassword} />
        <TextInput
          type="password"
          placeholder="Password Confirmation"
          isValid={registerPasswordConfirmationValid}
          {...registerPasswordConfirmation}
        />
        <SubmitButton
          fullwidth
          onClick={() => {
            checkRegisterInputsValid(registerEmial.value, registerPassword.value, registerPasswordConfirmation.value);
          }}
        >
          Submit
        </SubmitButton>
      </Form.Field>
    );
  }
};

export default LoginBody;
