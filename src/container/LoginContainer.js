import React from 'react';
import { Container, Columns, Section } from 'react-bulma-components/full';
import LoginHeader from '../components/Login/LoginHeader';
import LoginBody from '../components/Login/LoginBody';
import { LoginProvider } from '../context/LoginContext';

export default function LoginContainer() {
  return (
    <Section>
      <Container>
        <Columns centered>
          <Columns.Column size={8}>
            <LoginProvider>
              <LoginHeader />
              <LoginBody />
            </LoginProvider>
          </Columns.Column>
        </Columns>
      </Container>
    </Section>
  );
}
