import React from 'react';
import { Container, Columns, Section } from 'react-bulma-components/full';

export default function HomeContainer() {
  return (
    <Section>
      <Container>
        <Columns centered />
      </Container>
    </Section>
  );
}
